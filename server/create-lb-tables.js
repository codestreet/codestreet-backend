var server   = require('./server');
var ds       = server.dataSources.codestreetDS;
/*var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role', 'Batch',
  'Bug', 'BusinessInquiry', 'InstructorInquiry', 'Suggestion', 'SupportTicket',
 'UniversityInquiry', 'user', 'Course'];*/
var lbTables = ['Course'];
ds.autoupdate(lbTables, function (er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});
