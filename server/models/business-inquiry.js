'use strict';

module.exports = function (Businessinquiry) {

  Businessinquiry.definition.properties.created.default = function () {
    return new Date();
  };

  Businessinquiry.definition.properties.modified.default = function () {
    return new Date();
  };

  //send verification email after registration
  Businessinquiry.afterRemote('create', function (ctx, businessinquiry, next) {
    console.log('> user.afterRemote triggered');

    var app = Businessinquiry.app;

    const greeting    = `<p>Hi ${businessinquiry.name}, </p>`;
    const message     = `<p>Your message has been submitted Successfully</p>`;
    const description = `<p>${businessinquiry.description}</p>`;
    const signature   = `<p>Your friends at Codestreet! <br> Happy Learning!<p>`;

    const html = `${greeting}${message}${description}${signature}`;

    app.models.Email.send({
      to     : `${businessinquiry.email};${app.get('emailNoReply')}`,
      from   : app.get('emailNoReply'),
      subject: 'Message Submitted Successfully',
      html   : html
    }, function (err) {
      if (err) return console.log('> error sending business inquiry successful email', err);
      console.log('> sending business inquiry successful email to:', businessinquiry.email);
    });

    ctx.result = businessinquiry;

    next();
  });
};
