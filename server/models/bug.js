'use strict';

module.exports = function (Bug) {

  Bug.definition.properties.created.default = function () {
    return new Date();
  };

  Bug.definition.properties.modified.default = function () {
    return new Date();
  };

  //send verification email after registration
  Bug.afterRemote('create', function (ctx, bug, next) {
    console.log('> user.afterRemote triggered');

    var app = Bug.app;

    const greeting    = `<p>Hi ${bug.name}, </p>`;
    const message     = `<p>Your message has been submitted Successfully</p>`;
    const description = `<p>${bug.description}</p>`;
    const signature   = `<p>Your friends at Codestreet! <br> Happy Learning!<p>`;

    const html = `${greeting}${message}${description}${signature}`;

    app.models.Email.send({
      to     : `${bug.email};${app.get('emailNoReply')}`,
      from   : app.get('emailNoReply'),
      subject: 'Bug report Submitted Successfully',
      html   : html
    }, function (err) {
      if (err) return console.log('> error sending bug report successful email', err);
      console.log('> sending  bug report successful email to:', bug.email);
    });

    ctx.result = bug;

    next();
  });
};
