'use strict';

module.exports = function (Supportticket) {

  Supportticket.definition.properties.created.default = function () {
    return new Date();
  };

  Supportticket.definition.properties.modified.default = function () {
    return new Date();
  };

  //send verification email after registration
  Supportticket.afterRemote('create', function (ctx, supportticket, next) {
    console.log('> user.afterRemote triggered');

    var app = Supportticket.app;

    const greeting    = `<p>Hi ${supportticket.name}, </p>`;
    const message     = `<p>Your message has been submitted Successfully</p>`;
    const description = `<p>${supportticket.description}</p>`;
    const signature   = `<p>Your friends at Codestreet! <br> Happy Learning!<p>`;

    const html = `${greeting}${message}${description}${signature}`;

    app.models.Email.send({
      to     : `${supportticket.email};${app.get('emailNoReply')}`,
      from   : app.get('emailNoReply'),
      subject: 'Message Submitted Successfully',
      html   : html
    }, function (err) {
      if (err) return console.log('> error sending suppport ticket successful email', err);
      console.log('> sending support ticket successful email to:', supportticket.email);
    });

    ctx.result = supportticket;

    next();
  });
};
