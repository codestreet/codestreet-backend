'use strict';

module.exports = function (Instructorinquiry) {
  Instructorinquiry.definition.properties.created.default = function () {
    return new Date();
  };

  Instructorinquiry.definition.properties.modified.default = function () {
    return new Date();
  };

  //send verification email after registration
  Instructorinquiry.afterRemote('create', function (ctx, instructorinquiry, next) {
    console.log('> user.afterRemote triggered');

    var app = Instructorinquiry.app;

    const greeting    = `<p>Hi ${instructorinquiry.name}, </p>`;
    const message     = `<p>Your message has been submitted Successfully</p>`;
    const description = `<p>${instructorinquiry.description}</p>`;
    const signature   = `<p>Your friends at Codestreet! <br>  Happy Learning!<p>`;

    const html = `${greeting}${message}${description}${signature}`;

    app.models.Email.send({
      to     : `${instructorinquiry.email};${app.get('emailNoReply')}`,
      from   : app.get('emailNoReply'),
      subject: 'Message Submitted Successfully',
      html   : html
    }, function (err) {
      if (err) return console.log('> error sending instructor inquiry successful email', err);
      console.log('> sending instructor inquiry successful email to:', instructorinquiry.email);
    });

    ctx.result = instructorinquiry;

    next();
  });
};
