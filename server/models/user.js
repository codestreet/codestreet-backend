var path = require('path');

module.exports = function (User) {

  User.definition.properties.created.default = function () {
    return new Date();
  };

  User.definition.properties.modified.default = function () {
    return new Date();
  };

  //send verification email after registration
  User.afterRemote('create', function (ctx, user, next) {
    console.log('> user.afterRemote triggered');

    var app = User.app;

    var options = {
      type: 'email',
      to: user.email,
      from: 'codestreet@newshq.io',
      subject: 'Thanks for registering.',
      user: user,
      host: app.get('uiHost'),
      route: 'verify',
      protocol: 'http',
      port: app.get('uiPort'),
      text: 'Please activate your account by clicking on this link or copying and pasting it in a new browser window:\n\t{href}'
    };

    options.verifyHref = options.verifyHref ||
      options.protocol +
      '://' +
      options.host +
      ':' +
      options.port +
      "/" +
      options.route +
      '?uid=' +
      options.user.id;

    user.verify(options, function (err, response) {
      if (err) {
        User.deleteById(user.id);
        return next(err);
      }

      console.log('> verification email sent:', response);

      ctx.res.sendStatus(200);
    });
  });

  //send password reset link when requested
  User.on('resetPasswordRequest', function (info) {

    var app = User.app;
    var url = 'http://' + app.get('uiHost') + ':' + app.get('uiPort') + '/change-password';
    var link = url + '?access_token=' + info.accessToken.id;

    var html = '<p>Click <a href="' + link + '">here</a> to reset your password. </p>' +
      '<p>Alternatively copy & paste this link in your browser</p> ' + link;

    app.models.Email.send({
      to: info.email,
      from: app.get('emailNoReply'),
      subject: 'Password reset',
      html: html
    }, function (err) {
      if (err) return console.log('> error sending password reset email');
      console.log('> sending password reset email to:', info.email);
    });
  });
};
