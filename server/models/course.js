'use strict';

module.exports = function (Course) {

  Course.definition.properties.created.default = function () {
    return new Date();
  };

  Course.definition.properties.modified.default = function () {
    return new Date();
  };

};
