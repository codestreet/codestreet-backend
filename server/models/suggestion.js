'use strict';

module.exports = function (Suggestion) {

  Suggestion.definition.properties.created.default = function () {
    return new Date();
  };

  Suggestion.definition.properties.modified.default = function () {
    return new Date();
  };

  //send verification email after registration
  Suggestion.afterRemote('create', function (ctx, suggestion, next) {
    console.log('> user.afterRemote triggered');

    const app = Suggestion.app;

    const greeting    = `<p>Hi ${suggestion.name}, </p>`;
    const message     = `<p>Your message has been submitted Successfully</p>`;
    const description = `<p>${suggestion.description}</p>`;
    const signature   = `<p>Your friends at Codestreet! <br> Happy Learning!<p>`;

    const html = `${greeting}${message}${description}${signature}`;

    app.models.Email.send({
      to     : `${suggestion.email};${app.get('emailNoReply')}`,
      from   : app.get('emailNoReply'),
      subject: 'Message Submitted Successfully',
      html   : html
    }, function (err) {
      if (err) return console.log('> error sending suggestion successful email', err);
      console.log('> sending suggestion successful email to:', suggestion.email);
    });

    ctx.result = suggestion;

    next();
  });
};
