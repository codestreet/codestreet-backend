'use strict';

module.exports = function (Universityinquiry) {
  Universityinquiry.definition.properties.created.default = function () {
    return new Date();
  };

  Universityinquiry.definition.properties.modified.default = function () {
    return new Date();
  };

  //send verification email after registration
  Universityinquiry.afterRemote('create', function (ctx, universityInquiry, next) {
    console.log('> user.afterRemote triggered');

    var app = Universityinquiry.app;

    const greeting    = `<p>Hi ${universityInquiry.name}, </p>`;
    const message     = `<p>Your message has been submitted Successfully</p>`;
    const description = `<p>${universityInquiry.description}</p>`;
    const signature   = `<p>Your friends at Codestreet! <br> Happy Learning!<p>`;

    const html = `${greeting}${message}${description}${signature}`;

    app.models.Email.send({
      to     : `${universityInquiry.email};${app.get('emailNoReply')}`,
      from   : app.get('emailNoReply'),
      subject: 'Message Submitted Successfully',
      html   : html
    }, function (err) {
      if (err) return console.log('> error sending instructor inquiry successful email', err);
      console.log('> sending instructor inquiry successful email to:', universityInquiry.email);
    });

    ctx.result = universityInquiry;

    next();
  });
};
