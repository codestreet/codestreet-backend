'use strict';

module.exports = function (Batch) {

  Batch.definition.properties.created.default = function () {
    return new Date();
  };

  Batch.definition.properties.modified.default = function () {
    return new Date();
  };

};
