'use strict';

// to enable these logs set `DEBUG=boot:02-load-users` or `DEBUG=boot:*`
//var log = require('debug')('boot:02-load-users');

module.exports = function (app) {

  createDefaultUsers();

  function createDefaultUsers() {

    var User = app.models.User;
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;

    var users = [];
    var roles = [{
      name: 'admin',
      users: [{
        name: 'Admin',
        email: 'admin@codestreet.io',
        emailVerified: true,
        password: 'Himalaya123'
      }]
    }, {
      name: 'users',
      users: [{
        name: 'Guest',
        email: 'guest@codestreet.io',
        emailVerified: true,
        password: 'Himalaya123'
      }]
    }];

    roles.forEach(function (role) {
      Role.findOrCreate(
        {where: {name: role.name}}, // find
        {name: role.name}, // create
        function (err, createdRole, created) {
          if (err) {
            console.error('error running findOrCreate(' + role.name + ')', err);
          }

          role.users.forEach(function (roleUser) {
            User.findOrCreate(
              {where: {username: roleUser.username}}, // find
              roleUser, // create
              function (err, createdUser, created) {
                if (err) {
                  console.error('error creating roleUser', err);
                }

                createdRole.principals.create({
                  principalType: RoleMapping.USER,
                  principalId: createdUser.id
                }, function (err, rolePrincipal) {
                  if (err) {
                    console.error('error creating rolePrincipal', err);
                  }
                  users.push(createdUser);
                });
              });
          });
        });
    });

    RoleMapping.belongsTo(User);
    User.hasMany(RoleMapping, {foreignKey: 'principalId'});
    Role.hasMany(User, {through: RoleMapping, foreignKey: 'roleId'});

    return users;
  }

};
