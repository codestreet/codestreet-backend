var dsConfig = require('../datasources.json');

module.exports = function (app) {
  var User = app.models.user;
  var Course = app.model.course;

  //login page
  app.get('/', function (req, res) {
    res.sendStatus(200);
  });

  //reset the user's pasword
  app.post('/reset-password', function (req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);

    //verify passwords match
    if (!req.body.password || !req.body.confirmation ||
      req.body.password !== req.body.confirmation) {
      return res.sendStatus(400, new Error('Passwords do not match'));
    }

    User.findById(req.accessToken.userId, function (err, user) {
      if (err) return res.sendStatus(404);
      user.updateAttribute('password', req.body.password, function (err, user) {
        if (err) {
          return res.sendStatus(404);
        } else {
          console.log('> password reset processed successfully');
          res.sendStatus(200);
        }
      });
    });
  });
};
